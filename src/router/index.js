import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/detail',
    name: 'detail',
    component: () =>
      import(
        /* webpackChunkName: "detail" */
        '../views/Detail.vue'
      )
  },
  {
    path: '/about',
    name: 'about',
    // this generates a separate chunk (about.[hash].js) for this route which is
    // lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "about" */
        '../views/About.vue'
      )
  },
  {
    path: '/login',
    name: 'login',
    // this generates a separate chunk (about.[hash].js) for this route which is
    // lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "login" */
        '../views/Login.vue'
      )
  },
  {
    path: '/publish-inform',
    name: 'publishInform',
    // this generates a separate chunk (about.[hash].js) for this route which is
    // lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "about" */
        '../views/PublishInfom.vue'
      )
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
